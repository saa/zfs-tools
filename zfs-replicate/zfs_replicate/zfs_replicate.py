#!/usr/bin/env python

import argparse, getpass, inspect, logging, yaml, re, subprocess, threading, time, os
from logging import handlers as logging_handlers

try:
    # python 3
    from queue import Empty, Queue
except:
    # python 2
    from Queue import Empty, Queue


class ZFileSystem:
    """
    Abstracts basic ZFS filesystem operations
    """

    def __init__(
        self,
        name=None,
        pool=None,
        host="localhost",
        snapshots=None,
        retention=None,
        ssh_opts="",
        compressed_stream=False,
    ):
        self.name = name
        self.pool = pool
        self.host = host
        self.snapshots = []
        self.ssh_opts = ssh_opts
        self.compressed_stream = compressed_stream
        self.retention = retention
        self.bytes_sent = 0
        self.exists = self._filesystem_exists()
        # if we were given a list of snapshots, use that
        if isinstance(snapshots, list):
            self.snapshots = sorted(snapshots)
        # else if the filesystem exists, scan it for snapshots
        elif self.exists:
            self._snapshots()
        # else set an empty list
        else:
            self.snapshots = []

    def _runcmd(self, command, fatal=False):
        """
        runs a system command, wrapped in ssh
        returns the command output
        """
        cmd = re.split(
            "\s+", "/bin/ssh {} {} {}".format(self.ssh_opts, self.host, command)
        )
        output = []
        logger.debug("Command: {}".format(cmd))
        p = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True
        )
        p.wait()
        for stdout_line in iter(p.stdout.readline, ""):
            output.append(stdout_line.rstrip())
        if p.returncode > 0:
            if fatal:
                raise Exception(" ".join(output))
            logger.error(output)
        else:
            logger.debug(output)
        return output

    def _pipecmd(self, command1, command2, fatal=False):
        """
        pipe the output of command1 to command2
        returns the output of command2
        """
        logger.debug("Piped command: {} | {}".format(command1, command2))
        cmd1 = re.split("\s+", command1)
        cmd2 = re.split("\s+", command2)
        p1 = subprocess.Popen(cmd1, stdout=subprocess.PIPE)
        p2 = subprocess.Popen(cmd2, stdin=p1.stdout, stdout=subprocess.PIPE)
        output = p2.communicate()[0]
        if p1.returncode > 0 or p2.returncode > 0:
            if fatal:
                raise Exception(output)
            logger.error(output)
        else:
            logger.debug(output)
        return output

    def _snapshots(self):
        """
        populates self.snapshots with a list of snapshot name suffixes
        """
        logger.debug("Fetching snapshot list for {}".format(self.get_fullname()))
        output = self._runcmd(
            "/sbin/zfs list -H -t snapshot -o name -r {}".format(self.get_fullname()),
            fatal=True,
        )
        self.snapshots = []
        for line in output:
            m = re.match("{}@(.*)".format(self.get_fullname()), line)
            if m:
                self.snapshots.append(m.group(1))

    def _convert_str_to_bytes(self, string):
        """
        accepts a string such as "128K" and returns the integer value in (metric) bytes
        (e.g. 128000) or None if it cannot parse the string
        """
        result = None
        units = {
            "K": 1000,
            "M": 1000 ** 2,
            "G": 1000 ** 3,
            "T": 1000 ** 4,
            "P": 1000 ** 5,
            "E": 1000 ** 6,
        }
        stripped = re.sub(",", "", string)
        m = re.match(r"([0-9]+(\.[0-9]+)?)([BKMGTPE])?", stripped, re.IGNORECASE)
        if m:
            value = float(m.group(1))
            unit = m.group(3)
            if unit is None or unit.upper() == "B":
                result = int(value)
            else:
                try:
                    result = int(value * units[unit.upper()])
                except KeyError:
                    pass
        return result

    def _filesystem_exists(self):
        """
        checks to see if the filesystem exists
        """
        try:
            output = self._runcmd(
                "/sbin/zfs list -H -o name {}".format(self.get_fullname()), fatal=True
            )
            logger.debug(
                "Filesystem {} found on host {}".format(self.get_fullname(), self.host)
            )
        except Exception as e:
            logger.debug(
                "Filesystem {} not found on host {}: {}".format(
                    self.get_fullname(), self.host, e
                )
            )
            return False
        return True

    def has_snapshots(self):
        """
        checks if self.snapshots has been populated
        """
        return len(self.snapshots) > 0

    def get_last_snapshot(self):
        """
        gets the most recent snapshot, based on suffix
        """
        if self.exists:
            if not self.has_snapshots():
                self._snapshots()
            return sorted(self.snapshots)[-1]

    def get_fullname(self, snap_name=None):
        """
        returns the full pool/fs@suffix filesystem/snapshot label
        """
        if snap_name is None:
            return "{}/{}".format(self.pool, self.name)
        else:
            return "{}/{}@{}".format(self.pool, self.name, snap_name)

    def snapshot(self, name=None):
        """
        takes a snapshot of the filesystem and appends the suffix to self.snapshots
        """
        if name is None:
            name = "%Y%m%d"
        expanded_name = time.strftime(name)
        if expanded_name not in self.snapshots:
            logger.info(
                "Creating snapshot {} on host {}".format(
                    self.get_fullname(expanded_name), self.host
                )
            )
            self._runcmd(
                "/sbin/zfs snapshot {}".format(self.get_fullname(expanded_name))
            )
            self.snapshots.append(expanded_name)

    def rollback(self):
        """
        rolls back the filesystem to the last snapshot
        """
        if self.exists:
            last_snap = self.get_last_snapshot()
            logger.info(
                "Rolling back filesystem to {} on host {}".format(
                    self.get_fullname(snap_name=last_snap), self.host
                )
            )
            self._runcmd(
                "/sbin/zfs rollback -r {}".format(self.get_fullname(last_snap)),
                fatal=True,
            )
        else:
            logger.info(
                "Filesystem {} not found on {}. Skipping rollback".format(
                    self.get_fullname(), self.host
                )
            )

    def get_xfer_bytes(self, begin=None, end=None):
        """
        does a dry-run "send" and parses the transfer size from the output
        """
        bytes = 0
        # if we are not given an end snap, calculate the size of a non-incremental send
        if end is None:
            cmd = "/sbin/zfs send --replicate --parsable --dryrun {}".format(
                self.get_fullname(begin)
            )
        else:
            cmd = "/sbin/zfs send --replicate --parsable --dryrun -I {} {}".format(
                begin, self.get_fullname(end)
            )
        if self.compressed_stream:
            cmd = re.sub("zfs send", "zfs send --compressed", cmd, count=1)
        output = self._runcmd(cmd)
        # look for a line like "size   98765"
        for line in output:
            m = re.match("size\s+([0-9]+)", line)
            if m:
                bytes = int(m.group(1))
                break
        return bytes

    def send(self, name=None, destination=None, compress=False):
        """
        sends a snapshot to a remote filesystem
        """
        if name is None:
            name = self.get_last_snapshot()
        if destination is None:
            raise ValueError("destination cannot be None")
        my_last_snap = name
        if destination.exists:
            their_last_snap = destination.get_last_snapshot()
            xfer_bytes = self.get_xfer_bytes(begin=their_last_snap, end=my_last_snap)
            logger.info(
                "Our most recent snapshot is {}, theirs is {}. Sending {} bytes (compression: {})".format(
                    self.get_fullname(snap_name=my_last_snap),
                    destination.get_fullname(snap_name=their_last_snap),
                    xfer_bytes,
                    "ON" if self.compressed_stream else "OFF",
                )
            )
            # send incremental stream
            send_cmd = "/bin/ssh {} {} /sbin/zfs send --replicate -I {} {}".format(
                self.ssh_opts,
                self.host,
                their_last_snap,
                self.get_fullname(my_last_snap),
            )
        else:
            xfer_bytes = self.get_xfer_bytes(begin=my_last_snap, end=None)
            logger.info(
                "Destination filesystem {} does not exist. Performing intial sync".format(
                    destination.get_fullname()
                )
            )
            # send initial snapshot
            send_cmd = "/bin/ssh {} {} /sbin/zfs send --replicate {}".format(
                self.ssh_opts,
                self.host,
                self.get_fullname(my_last_snap),
            )
        recv_cmd = "/bin/ssh {} {} /sbin/zfs recv {}".format(
            self.ssh_opts, destination.host, destination.get_fullname()
        )
        if self.compressed_stream:
            send_cmd = re.sub("zfs send", "zfs send --compressed", send_cmd, count=1)
        self._pipecmd(send_cmd, recv_cmd, fatal=True)
        # update the snapshot list on the destination
        destination._snapshots()
        self.bytes_sent = self.bytes_sent + xfer_bytes

    def destroy(self, name):
        """
        recursively destroys a filesystem/snapshot
        """
        if not self.has_snapshots():
            self._snapshots()
        if name in self.snapshots:
            logger.info("Destroying {}".format(self.get_fullname(snap_name=name)))
            self._runcmd(
                "/sbin/zfs destroy -r {}".format(self.get_fullname(snap_name=name))
            )

    def expire_snapshots(self):
        """
        destroys snapshots beyond configured retention count
        """
        if self.retention is not None:
            expired = sorted(self.snapshots)[: (self.retention * -1)]
            if len(expired):
                logger.info(
                    "The following snapshots on {} are past the limit of {}: {}".format(
                        self.host,
                        self.retention,
                        [self.get_fullname(snap_name=e) for e in expired],
                    )
                )
                for e in expired:
                    self.destroy(e)


def load_config(filename):
    with open(filename, "r") as file:
        conf = yaml.load(file, Loader=yaml.Loader)
        conf.update(load_subconfigs(conf))
        filesystems = []
        default = conf.get("default", {})
        for fs in conf["filesystems"]:
            # populate missing values in "source" with defaults
            fs.setdefault("source", default.get("source", {}).copy())
            for key in default["source"].keys():
                fs["source"].setdefault(key, default["source"][key])
            fs.setdefault("destination", default.get("destination", {}).copy())
            for key in default["destination"].keys():
                fs["destination"].setdefault(key, default["destination"][key])
            # make sure we have all the required values, else die
            try:
                fs["name"]
            except KeyError:
                logger.error(
                    "FATAL: Missing configuration value 'name' in filesystem config"
                )
                exit(255)
            try:
                fs["source"]["host"]
                fs["source"]["pool"]
                fs["source"]["retention"]
                fs["destination"]["host"]
                fs["destination"]["pool"]
                fs["destination"]["retention"]
            except KeyError as e:
                logger.error(
                    "FATAL: Missing configuration value {} for filesystem '{}'".format(
                        e, fs["name"]
                    )
                )
                exit(255)
            # use global fs name if not specified in source or destination
            if "name" not in fs["source"]:
                fs["source"]["name"] = fs["name"]
            if "name" not in fs["destination"]:
                fs["destination"]["name"] = fs["name"]
            # looks good!
            filesystems.append(fs)
        new_conf = conf
        new_conf["snap_format"] = conf.get("snap_format", "%Y%m%d")
        new_conf["concurrent_jobs"] = conf.get("concurrent_jobs", 1)
        new_conf["ssh_opts"] = conf.get("ssh_opts", "")
        new_conf["log_level"] = conf.get("log_level", "INFO")
        new_conf["compressed"] = conf.get("compressed", False)
        new_conf["filesystems"] = filesystems
        return new_conf


def load_subconfigs(conf):
    """
    Load other config files specified with the "include" key
    """
    include_dir = conf.get("include", False)
    new_conf = conf.copy()
    if include_dir:
        for root, dirs, files in os.walk(include_dir):
            for filename in sorted(files):
                if re.search(r"\.yml$", filename):
                    with open(root + filename, "r") as file:
                        subconf = yaml.load(file, Loader=yaml.Loader)
                        new_conf.update(merge_configs(new_conf, subconf))
    return new_conf


def merge_configs(a, b):
    """
    Overlay a on top of b using a recursive merge as c
    """
    if isinstance(a, dict):
        c = a.copy()
        if isinstance(b, dict):
            for key, value in b.iteritems():
                if key in c:
                    c[key] = merge_configs(c[key], value)
                else:
                    c[key] = value
    elif isinstance(a, list):
        c = list(a + b)
    else:
        c = b
    return c


def setup_logger(conf):
    """
    configures logging handlers
    """
    formatter = logging.Formatter("%(asctime)s %(levelname)s\t%(message)s")
    if "log_file" in conf:
        log_handler = logging.FileHandler(conf["log_file"])
    else:
        log_handler = logging.StreamHandler()
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)
    if "log_level" in conf:
        # set logging to level defined log_level
        logger.setLevel(eval("logging.{}".format(conf["log_level"])))
    if "mail" in conf:
        # configure email settings
        smtp_server = conf["mail"].get("smtp_server", "localhost")
        subject = conf["mail"].get("subject", "ZFS Backup Report")
        sender = conf["mail"].get("sender", getpass.getuser())
        recipients = conf["mail"].get("recipients", [])
        log_level = conf["mail"].get("log_level", "ERROR")
        if len(recipients):
            smtpHandler = logging_handlers.SMTPHandler(
                smtp_server, sender, recipients, subject
            )
            smtpHandler.setLevel(eval("logging.{}".format(log_level)))
            smtpHandler.setFormatter(formatter)
            logger.addHandler(smtpHandler)


def elapsed(start, end, precision=2):
    return round(end - start, precision)


def convert_bytes_to_mbytes(bytes, precision=2):
    """
    converts an integer byte value to a float of metric megabytes
    """
    return round(float(bytes) / 1000 ** 2, precision)


def hosts_in_sync(source, dest):
    """
    returns True if both hosts have the same last snapshot name
    """
    return source.get_last_snapshot() == dest.get_last_snapshot()


def replication_worker(queue, conf, stop):
    """
    pulls jobs from a queue and processes them
    """
    # run until stopped
    while stop.is_set() is False:
        start_time = time.time()
        try:
            fs = (
                queue.get_nowait()
            )  # throws Queue.Empty exception if nothing is returned
            logger.info(
                "Processing fs '{}/{}' on {}".format(
                    fs["source"]["pool"], fs["source"]["name"], fs["source"]["host"]
                )
            )
            source = ZFileSystem(
                name=fs["source"]["name"],
                pool=fs["source"]["pool"],
                host=fs["source"]["host"],
                retention=fs["source"]["retention"],
                ssh_opts=conf["ssh_opts"],
                compressed_stream=conf["compressed"],
            )
            dest = ZFileSystem(
                name=fs["destination"]["name"],
                pool=fs["destination"]["pool"],
                host=fs["destination"]["host"],
                retention=fs["destination"]["retention"],
                ssh_opts=conf["ssh_opts"],
                compressed_stream=conf["compressed"],
            )
            if source.exists:
                # snapshot the source
                source.snapshot(name=conf.get("snap_format", None))
                if not hosts_in_sync(source, dest):
                    # roll back the destination to its most recent snap
                    dest.rollback()
                    # send the latest snapshot(s) to the destination
                    source.send(destination=dest)
                    # destroy old snapshots
                    source.expire_snapshots()
                    dest.expire_snapshots()
                    logger.info(
                        "Synchronized {}:{} -> {}:{} in {} seconds ({} MB)".format(
                            source.host,
                            source.get_fullname(),
                            dest.host,
                            dest.get_fullname(),
                            elapsed(start_time, time.time()),
                            convert_bytes_to_mbytes(source.bytes_sent),
                        )
                    )
            else:
                logger.error(
                    "Filesystem '{}/{}' not detected on {} - skipping".format(
                        fs["source"]["pool"],
                        fs["source"]["name"],
                        fs["source"]["host"],
                    )
                )
            queue.task_done()
        # stop when there are no more items in the queue
        except Empty:
            logger.debug("No more items in queue. Shutting down worker threads.")
            stop.set()
        # log exceptions and move on to the next job
        except Exception as e:
            func = inspect.currentframe().f_back.f_code
            logger.error("({}: {}) {}".format(func.co_name, func.co_firstlineno, e))


def main():
    start_time = time.time()
    job_q = Queue()
    threads = []
    threads_stop = threading.Event()

    try:
        parser = argparse.ArgumentParser(description="Replicate ZFS filesystems")
        parser.add_argument(
            "-c",
            "--config-file",
            help="configuration file",
            default="/etc/zfs-replicate.yml",
        )
        parser.add_argument(
            "--validate",
            help="validate config file and exit",
            action="store_true",
            default=False,
        )
        args = parser.parse_args()
        # if --validate was passed, stop here
        if args.validate:
            logging.basicConfig()
            conf = load_config(args.config_file)
            load_subconfigs(conf)
            print(yaml.dump(conf))
            print("OK!")
            exit(0)
        # load config file
        conf = load_config(args.config_file)
        # set up logging
        setup_logger(conf)
        logger.info("Starting up")
        logger.info("Log level set to {}".format(conf["log_level"]))
        logger.info("Concurrent worker threads: {}".format(conf["concurrent_jobs"]))
        # queue up each configured filesystem
        for fs in conf["filesystems"]:
            job_q.put(fs)
        # spawn worker threads
        for t in range(conf["concurrent_jobs"]):
            thread = threading.Thread(
                target=replication_worker,
                args=(job_q, conf, threads_stop),
                name="thread_{}".format(t),
            )
            thread.daemon = True
            logger.debug("Spawing worker thread: {}".format(thread.name))
            thread.start()
            threads.append(thread)
    # try to stop threads gracefully on Ctrl+C
    except KeyboardInterrupt:
        stop.set()
    for t in threads:
        t.join()
    logger.info("Total runtime: {} seconds".format(elapsed(start_time, time.time())))


logger = logging.getLogger()


if __name__ == "__main__":
    main()
