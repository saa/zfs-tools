#!/usr/bin/env python

"""
A nagios plugin to check if ZFS snapshots sent by zfs-replicate.py are current

Takes the path to the zfs-replicate config file as an argument

https://gitlab.uvm.edu/saa/zfs-tools/tree/master/zfs-replicate
"""
import re, subprocess, sys, time, yaml
from os import listdir
from os.path import isfile


def get_default(config_file):
    """
    returns the "default" stanza from the config file, or an empty dict
    """
    conf = load_config(config_file)
    return conf.get("default", {})


def scan_configs(config_file):
    """
    finds additional configs specified by the "include" parameter
    """
    file_list = [config_file]
    conf = load_config(config_file)
    if "include" in conf:
        for file in listdir(conf["include"]):
            file_list.append(conf["include"] + "/" + file)
    return file_list


def load_config(config_file):
    with open(config_file, "r") as file:
        conf = yaml.load(file, Loader=yaml.Loader)
    return conf


def get_snapshot_list(config_file, default):
    """
    parses the config and returns a list of snapshots with a destination of 'localhost'
    """
    snapshots = []
    conf = load_config(config_file)
    snap_format = conf.get("snap_format", "%Y%m%d")
    if "filesystems" in conf:
        for fs in conf["filesystems"]:
            for stanza in ["source", "destination"]:
                # if 'source/destination' is missing, fill in the defaults
                if stanza not in fs:
                    fs[stanza] = default.get(stanza, {}).copy()
                # else fill in any missing fields with default
                elif stanza in default:
                    for key in default[stanza].keys():
                        if not key in fs[stanza]:
                            fs[stanza][key] = default[stanza].get(key)
                # set global 'name' to 'source/destination' name
                if "name" not in fs[stanza]:
                    fs[stanza]["name"] = fs["name"]
            # if the host is 'localhost' assemble a snapshot name and add it
            if fs[stanza]["host"] == "localhost":
                snapshots.append(
                    "{}/{}@{}".format(
                        fs[stanza]["pool"],
                        fs[stanza]["name"],
                        time.strftime(snap_format),
                    )
                )
    return snapshots


def exists(snapshot):
    """
    returns True if the snapshot exists, else False
    """
    return (
        True
        if subprocess.call(
            re.split("\s+", "/sbin/zfs list -t snapshot {}".format(snapshot)),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        == 0
        else False
    )


def __main__():
    missing = []
    exit_code = 3
    base_config_file = sys.argv[1]
    # get default values from base config
    default = get_default(base_config_file)
    # assemble a list of missing snapshots
    config_files = scan_configs(base_config_file)
    for file in config_files:
        for snap in get_snapshot_list(file, default):
            if not exists(snap):
                missing.append(snap)
    if len(missing):
        print("CRITICAL: Missing current snapshots: {}".format(" ".join(missing)))
        exit_code = 2
    else:
        print("OK: All snapshots present")
        exit_code = 0
    exit(exit_code)


__main__()
