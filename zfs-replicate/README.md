# zfs-replicate

Easily replicate/backup ZFS filesystems via snapshot send/receive.

## Authentication and Authorization
This script runs ZFS system commands via SSH (even on localhost) and assumes the
pre-existence of keys for authentication

To run this script as a non-root user, you must grant some ZFS permissions:

On the sending server:

    zfs allow <user> destroy,hold,mount,send,snapshot <pool>

On the receiving server:

    zfs allow <user> create,destroy,hold,mount,receive,rollback[,<property>...] <pool>

## Configuration
The script is controlled via a YAML-formatted configuration file which specifies the
filesystems to be replicated, as well as the retention thresholds. Most parameters will
default to a hard-coded value if not specified.

The `snap_format` parameter controls the suffix that will be used when creating
snapshots. This uses `strftime()` style formatting to generate sequential snapshot
names.

    snap_format: "%Y%m%d"

Filesystems to replicate are defined under `filesystems`. You can set your preferred
default values under `default` and omit these settings in the filesystem stanzas,
however at a bare-minimum each filesystem must be defined by a `name` which must match
the name of the ZFS filesystem you wish to replicate. The script will preserve a number
of snapshots equal to `retention` and delete the oldest snaps first. You may set
separate retention thresholds on the source and destination if you wish.

    default:
      source:
        host: localhost
        pool: tank
        retention: 30
      destination:
        host: replica-server
        pool: pool0
        retention: 90

    filesystems:
      - name: example1
        destination:
          pool: someotherpool


See the included example file for more details.

Passing `--validate` to the script will cause to to process the config file and exit,
showing any errors.

## Usage

The script takes a single argument, `--config-file` which should point to the location
of your config. If you don't specify a file, it looks for `/etc/zfs-replicate.yml`. All
other options are controlled by the config file.

Typical usage would be to run the script via cron at whatever replication interval you
wish. Be sure to make `snap_format` granular enough to produce unique snapshot names for
your chosen frequency.

## Monitoring
There is an included `check_snapshots.py` nagios plugin which checks that filesystems on
"localhost" are present and current.
