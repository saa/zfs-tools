import getpass, logging, re, subprocess, threading, time, unittest
from zfs_replicate.zfs_replicate import *

try:
    # python 3
    from unittest import mock
except ImportError:
    # python 2
    from mock import mock

try:
    # python 3
    from queue import Queue
except:
    # python 2
    from Queue import Queue

# tell the logger to stfu
logging.basicConfig()
logging.disable(logging.ERROR)


class TestZFileSystem(unittest.TestCase):
    class fakePopen:
        def __init__(self, returncode=0):
            self.returncode = returncode
            self.stdout = open("/dev/null")

        def wait(self):
            pass

        def communicate(self):
            return ["test output"]

    def get_last_snapshot(self):
        """
        yeilds 00000000, 00000001, 00000002...
        """
        current = 0
        while True:
            yield str(current).zfill(8)
            current += 1

    @mock.patch.object(ZFileSystem, "_filesystem_exists")
    @mock.patch.object(ZFileSystem, "_snapshots")
    def test___init__(self, mock__snapshots, mock_fs_exists):
        mock_fs_exists.return_value = True
        default = ZFileSystem()
        self.assertIsNone(default.name)
        self.assertIsNone(default.pool)
        self.assertEqual(default.host, "localhost")
        self.assertEqual(default.snapshots, [])
        self.assertEqual(default.ssh_opts, "")
        self.assertIsNone(default.retention)
        mock__snapshots.assert_called_with()
        custom = ZFileSystem(
            name="test",
            pool="tank",
            host="example.com",
            snapshots=["00000000"],
            retention=30,
            ssh_opts="-e none",
        )
        self.assertEqual(custom.name, "test")
        self.assertEqual(custom.pool, "tank")
        self.assertEqual(custom.host, "example.com")
        self.assertEqual(custom.snapshots, ["00000000"])
        self.assertEqual(custom.retention, 30)
        self.assertEqual(custom.ssh_opts, "-e none")

    @mock.patch.object(subprocess, "PIPE")
    @mock.patch("subprocess.Popen")
    @mock.patch.object(ZFileSystem, "_snapshots")
    def test__runcmd(self, mock_snapshots, mock_popen, mock_pipe):
        mock_popen.return_value = self.fakePopen()
        zfs = ZFileSystem()
        self.assertEqual(zfs._runcmd("zfs list -t snapshot", fatal=True), [])
        self.assertRegexpMatches(
            str(mock_popen.call_args),
            r"^call\(\['/bin/ssh', 'localhost', 'zfs', 'list', '-t', 'snapshot'\],",
        )
        mock_popen.return_value = self.fakePopen(returncode=1)
        with self.assertRaises(Exception):
            zfs._runcmd("zfs list -t snapshot", fatal=True)

    @mock.patch.object(ZFileSystem, "_filesystem_exists")
    @mock.patch.object(subprocess, "PIPE")
    @mock.patch("subprocess.Popen")
    @mock.patch.object(ZFileSystem, "_snapshots")
    def test__pipecmd(self, mock_snapshots, mock_popen, mock_pipe, mock_fs_exists):
        mock_fs_exists.return_value = True
        mock_popen.return_value = self.fakePopen()
        zfs = ZFileSystem()
        self.assertEqual(
            zfs._pipecmd(
                "zfs send test/example@00000000", "zfs recv test/example", fatal=True
            ),
            "test output",
        )
        self.assertRegexpMatches(
            str(mock_popen.call_args_list[0]),
            r"^call\(\['zfs', 'send', 'test\/example@00000000'\],",
        )
        self.assertRegexpMatches(
            str(mock_popen.call_args_list[1]),
            r"^call\(\['zfs', 'recv', 'test\/example'\],",
        )
        mock_popen.return_value = self.fakePopen(returncode=1)
        with self.assertRaises(Exception):
            zfs._pipecmd(
                "zfs send test/example@00000000", "zfs recv test/example", fatal=True
            )

    @mock.patch.object(ZFileSystem, "_runcmd")
    def test__snapshots(self, mock_runcmd):
        mock_runcmd.return_value = ["test/example@00000000"]
        zfs = ZFileSystem(name="example", pool="test", snapshots=[])
        zfs._snapshots()
        self.assertEqual(zfs.snapshots, ["00000000"])
        self.assertRegexpMatches(
            str(mock_runcmd.call_args),
            r"^call\('\/sbin\/zfs list -H -t snapshot -o name -r test/example', fatal=True\)",
        )

    def test__convert_string_to_bytes(self):
        zfs = ZFileSystem(snapshots=[])
        self.assertEqual(zfs._convert_str_to_bytes("10"), 10)
        self.assertEqual(zfs._convert_str_to_bytes("10K"), 10000)
        self.assertEqual(zfs._convert_str_to_bytes("10.1M"), 10100000)
        self.assertEqual(zfs._convert_str_to_bytes("10,000B"), 10000)

    def test_has_snapshots(self):
        zfs1 = ZFileSystem(snapshots=[])
        self.assertFalse(zfs1.has_snapshots())
        zfs2 = ZFileSystem(snapshots=["00000000"])
        self.assertTrue(zfs2.has_snapshots())

    @mock.patch.object(ZFileSystem, "_filesystem_exists")
    @mock.patch.object(ZFileSystem, "_snapshots")
    def test_get_last_snapshot(self, mock_snapshots, mock_fs_exists):
        mock_fs_exists.return_value = True
        zfs1 = ZFileSystem(snapshots=[])
        try:
            zfs1.get_last_snapshot()
        except IndexError:
            pass
        mock_snapshots.assert_called()
        zfs2 = ZFileSystem(snapshots=["00000000"])
        self.assertEqual(zfs2.get_last_snapshot(), "00000000")

    def test_get_fullname(self):
        # set snapshots=[] to avoid system calls
        zfs = ZFileSystem(name="example", pool="test", snapshots=[])
        self.assertEqual(zfs.get_fullname(), "test/example")
        self.assertEqual(
            zfs.get_fullname(snap_name="00000000"), "test/example@00000000"
        )

    @mock.patch.object(ZFileSystem, "_runcmd")
    def test_snapshot(self, mock_runcmd):
        zfs = ZFileSystem(name="example", pool="test", snapshots=["00000000"])
        zfs.snapshot(name="00000000")
        mock_runcmd.assert_called_with(
            "/sbin/zfs list -H -o name test/example", fatal=True
        )
        zfs.snapshot(name="11111111")
        mock_runcmd.assert_called_with("/sbin/zfs snapshot test/example@11111111")
        zfs.snapshot()
        mock_runcmd.assert_called_with(
            "/sbin/zfs snapshot test/example@{}".format(time.strftime("%Y%m%d"))
        )

    @mock.patch.object(ZFileSystem, "_runcmd")
    def test_rollback(self, mock_runcmd):
        zfs = ZFileSystem(name="example", pool="test", snapshots=["00000000"])
        zfs.rollback()
        mock_runcmd.assert_called_with(
            "/sbin/zfs rollback -r test/example@00000000", fatal=True
        )

    @mock.patch.object(ZFileSystem, "get_last_snapshot")
    @mock.patch.object(ZFileSystem, "_runcmd")
    @mock.patch.object(ZFileSystem, "_pipecmd")
    def test_send(self, mock_pipecmd, mock_runcmd, mock_get_last_snapshot):
        mock_get_last_snapshot.side_effect = self.get_last_snapshot()
        source = ZFileSystem(
            name="example",
            pool="test",
            host="localhost",
            snapshots=["00000001"],
            compressed_stream=True,
        )
        destination = ZFileSystem(
            name="example", pool="test", host="remotehost", snapshots=[]
        )
        # with name and destination specified
        source.send(name="00000001", destination=destination)
        mock_pipecmd.assert_called_with(
            "/bin/ssh  localhost /sbin/zfs send --compressed --replicate -I 00000000 test/example@00000001",
            "/bin/ssh  remotehost /sbin/zfs recv test/example",
            fatal=True,
        )
        # with destination only, and compressed_stream == False
        mock_pipecmd.reset_mock()
        source.compressed_stream = False
        source.send(destination=destination)
        mock_pipecmd.assert_called_with(
            "/bin/ssh  localhost /sbin/zfs send --replicate -I 00000002 test/example@00000001",
            "/bin/ssh  remotehost /sbin/zfs recv test/example",
            fatal=True,
        )
        # with name only
        with self.assertRaises(ValueError):
            source.send(name="00000001")

    @mock.patch.object(ZFileSystem, "_snapshots")
    @mock.patch.object(ZFileSystem, "_runcmd")
    def test_destroy(self, mock_runcmd, mock_snapshots):
        zfs1 = ZFileSystem(
            name="example", pool="test", host="remotehost", snapshots=["00000000"]
        )
        zfs1.destroy("00000000")
        mock_runcmd.assert_called_with("/sbin/zfs destroy -r test/example@00000000")
        # make sure snapshot list is fetched if not set
        mock_runcmd.reset_mock()
        zfs2 = ZFileSystem(name="example", pool="test", host="remotehost", snapshots=[])
        zfs2.destroy("00000000")
        mock_snapshots.assert_called()

    @mock.patch.object(ZFileSystem, "destroy")
    def test_expire_snapshots(self, mock_destroy):
        zfs1 = ZFileSystem(name="example", pool="test", host="remotehost", snapshots=[])
        zfs1.expire_snapshots()
        mock_destroy.assert_not_called()
        zfs2 = ZFileSystem(
            name="example",
            pool="test",
            host="remotehost",
            snapshots=["00000000", "00000001", "00000002, 00000003"],
            retention=1,
        )
        zfs2.expire_snapshots()
        self.assertIn(mock.call("00000000"), mock_destroy.call_args_list)
        self.assertIn(mock.call("00000001"), mock_destroy.call_args_list)
        self.assertNotIn(mock.call("00000002"), mock_destroy.call_args_list)
        self.assertNotIn(mock.call("00000003"), mock_destroy.call_args_list)

    @mock.patch.object(ZFileSystem, "_runcmd")
    def test_get_xfer_bytes(self, mock_runcmd):
        mock_runcmd.return_value = ["garbage", "garbage", "size\t8675309"]
        zfs = ZFileSystem(name="example", pool="test", snapshots=[])
        self.assertEqual(zfs.get_xfer_bytes("00000000", "00000001"), 8675309)
        mock_runcmd.return_value = ["garbage", "garbage", "more garbage"]
        self.assertEqual(zfs.get_xfer_bytes("00000000", "00000001"), 0)


class TestMiscFunctions(unittest.TestCase):
    def regex_in(self, regex, list):
        """
        returns true if a list element matching the regex is found
        """
        for i in list:
            m = re.search(re.compile(regex), str(i))
            if m:
                return True
        return False

    def setUp(self):
        self.bare_conf = {"filesystems": []}
        self.full_conf = {
            "snap_format": "test snap_format",
            "ssh_opts": "test ssh_opts",
            "log_file": "test file",
            "log_level": "DEBUG",
            "concurrent_jobs": 999,
            "compressed": True,
            "mail": {
                "smtp_server": "test smtp_server",
                "subject": "test subject",
                "sender": "test sender",
                "recipients": ["test recipient"],
                "log_level": "DEBUG",
            },
            "default": {
                "source": {
                    "host": "localhost",
                    "pool": "source pool",
                    "retention": 999,
                },
                "destination": {
                    "host": "desthost",
                    "pool": "dest pool",
                    "retention": 999,
                },
            },
            "filesystems": [
                {
                    "name": "test",
                    "source": {
                        "host": "test host",
                        "pool": "test pool",
                        "retention": 3,
                    },
                    "destination": {
                        "host": "test dest",
                        "pool": "test pool",
                        "retention": 5,
                    },
                },
                {"name": "test2"},
            ],
        }
        self.repl_conf = {
            "filesystems": [
                {
                    "source": {
                        "host": "test host",
                        "name": "test",
                        "pool": "test pool",
                        "retention": 3,
                    },
                    "destination": {
                        "host": "test dest",
                        "name": "test",
                        "pool": "test pool",
                        "retention": 5,
                    },
                    "name": "test",
                }
            ],
            "ssh_opts": "test ssh_opts",
            "compressed": True,
        }

    @mock.patch.object(yaml, "load")
    @mock.patch("zfs_replicate.zfs_replicate.open")
    def test_load_config(self, mock_open, mock_yaml_load):
        # with the minimum possible config
        mock_yaml_load.return_value = self.bare_conf
        bare_conf = load_config("test")
        mock_open.assert_called_with("test", "r")
        self.assertEqual(
            bare_conf,
            {
                "snap_format": "%Y%m%d",
                "ssh_opts": "",
                "log_level": "INFO",
                "concurrent_jobs": 1,
                "filesystems": [],
                "compressed": False,
            },
        )
        # with a fully fleshed config
        mock_open.reset_mock()
        mock_yaml_load.return_value = self.full_conf
        full_conf = load_config("test")
        mock_open.assert_called_with("test", "r")
        self.assertEqual(
            full_conf,
            {
                "snap_format": "test snap_format",
                "filesystems": [
                    {
                        "source": {
                            "host": "test host",
                            "name": "test",
                            "pool": "test pool",
                            "retention": 3,
                        },
                        "destination": {
                            "host": "test dest",
                            "name": "test",
                            "pool": "test pool",
                            "retention": 5,
                        },
                        "name": "test",
                    },
                    {
                        "source": {
                            "host": "localhost",
                            "name": "test2",
                            "pool": "source pool",
                            "retention": 999,
                        },
                        "destination": {
                            "host": "desthost",
                            "name": "test2",
                            "pool": "dest pool",
                            "retention": 999,
                        },
                        "name": "test2",
                    },
                ],
                "log_level": "DEBUG",
                "concurrent_jobs": 999,
                "default": {
                    "source": {
                        "host": "localhost",
                        "pool": "source pool",
                        "retention": 999,
                    },
                    "destination": {
                        "host": "desthost",
                        "pool": "dest pool",
                        "retention": 999,
                    },
                },
                "mail": {
                    "sender": "test sender",
                    "log_level": "DEBUG",
                    "smtp_server": "test smtp_server",
                    "recipients": ["test recipient"],
                    "subject": "test subject",
                },
                "log_file": "test file",
                "ssh_opts": "test ssh_opts",
                "compressed": True,
            },
        )

    @mock.patch("logging.handlers.SMTPHandler")
    @mock.patch("logging.StreamHandler")
    @mock.patch("logging.FileHandler")
    def test_setup_logger(self, mock_filehandler, mock_streamhandler, mock_smtphandler):
        # with full config
        setup_logger(self.full_conf)
        mock_filehandler.assert_called_with("test file")
        mock_streamhandler.assert_not_called()
        mock_smtphandler.assert_called_with(
            "test smtp_server", "test sender", ["test recipient"], "test subject"
        )
        # with minimal mail config
        mock_smtphandler.reset_mock()
        mail_conf = dict(self.bare_conf)
        mail_conf["mail"] = {"recipients": ["test@example.com"]}
        setup_logger(mail_conf)
        mock_smtphandler.assert_called_with(
            "localhost", getpass.getuser(), ["test@example.com"], "ZFS Backup Report"
        )
        # with bare minimum config
        mock_filehandler.reset_mock()
        mock_streamhandler.reset_mock()
        mock_smtphandler.reset_mock()
        setup_logger(self.bare_conf)
        mock_streamhandler.assert_called_with()
        mock_filehandler.assert_not_called()
        mock_smtphandler.assert_not_called()

    def test_elapsed(self):
        self.assertEqual(elapsed(1, 10), 9.00)

    def test_convert_bytes_to_mbytes(self):
        self.assertEqual(convert_bytes_to_mbytes(10000000), 10.0)
        self.assertIsInstance(convert_bytes_to_mbytes(10000000), float)

    @mock.patch.object(ZFileSystem, "get_last_snapshot")
    def test_hosts_in_sync(self, mock_get_last_snapshot):
        zfs1 = ZFileSystem(name="example1", pool="test", snapshots=[])
        zfs2 = ZFileSystem(name="example2", pool="test", snapshots=[])
        mock_get_last_snapshot.side_effect = ["00000000", "00000000"]
        self.assertTrue(hosts_in_sync(zfs1, zfs2))
        mock_get_last_snapshot.side_effect = ["00000000", "00000001"]
        self.assertFalse(hosts_in_sync(zfs1, zfs2))

    @mock.patch("zfs_replicate.zfs_replicate.hosts_in_sync")
    @mock.patch("zfs_replicate.zfs_replicate.ZFileSystem", autospec=True)
    def test_replication_worker(self, mock_zfs, mock_hosts_in_sync):
        mock_zfs.return_value.exists = True
        mock_hosts_in_sync.return_value = False
        queue = Queue()
        stop = threading.Event()
        for fs in self.repl_conf["filesystems"]:
            queue.put(fs)
        replication_worker(queue, self.repl_conf, stop)
        self.assertIn(
            mock.call(
                host="test host",
                name="test",
                pool="test pool",
                retention=3,
                ssh_opts="test ssh_opts",
                compressed_stream=True,
            ),
            mock_zfs.call_args_list,
        )
        self.assertIn(
            mock.call(
                host="test dest",
                name="test",
                pool="test pool",
                retention=5,
                ssh_opts="test ssh_opts",
                compressed_stream=True,
            ),
            mock_zfs.call_args_list,
        )
        self.assertEqual(
            mock_zfs.method_calls.count(mock.call().snapshot(name=None)), 1
        )
        self.assertEqual(mock_zfs.method_calls.count(mock.call().rollback()), 1)
        self.assertEqual(mock_zfs.method_calls.count(mock.call().expire_snapshots()), 2)
        self.assertTrue(
            self.regex_in(r"^call\(\)\.send\(destination=", mock_zfs.method_calls)
        )


if __name__ == "__main__":
    unittest.main()
